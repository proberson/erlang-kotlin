import com.ericsson.otp.erlang.*

fun main(args: Array<String>) {
    System.setProperty("OtpConnection.trace", "0")
    val kotlinNode = OtpNode("kNode", "erlkotlin")
    if (kotlinNode.ping("eNode", 10000)) {
        System.out.println("eNode is up")
        val kProcess = kotlinNode.createMbox()
        val kPid = kProcess.self()
        kProcess.registerName("kProcess")

        // get value of my_counter
        val getAtom = OtpErlangAtom("get")
        val counterName = OtpErlangList("my_counter")
        val getMsg = arrayOf<OtpErlangObject>(kPid, getAtom, counterName)
        kProcess.send("erlcounters", "eNode", OtpErlangTuple(getMsg))
        var response = kProcess.receive()

        if (response is OtpErlangTuple) {
            val remoteCounterName = (response.elementAt(1) as OtpErlangString)
                .stringValue()
            val remoteCounterValue = (response.elementAt(2) as OtpErlangLong)
                .intValue()
            System.out.println(
                remoteCounterName + " = " +
                        remoteCounterValue)
        }

        // set my_counter to 1000
        var aValue: OtpErlangObject = OtpErlangLong(1000)

        val setMsg = arrayOf<OtpErlangObject>(kPid, OtpErlangAtom("set"), counterName, aValue)
        kProcess.send("erlcounters", "eNode", OtpErlangTuple(setMsg))
        response = kProcess.receive()
        if (response is OtpErlangTuple) {
            val remoteCounterName = response.elementAt(1)
                .toString()
            val remoteCounterValue = response.elementAt(2)
                .toString()
            System.out.println(
                remoteCounterName + " = " +
                        remoteCounterValue)
        }

        // increment my_counter by 100
        aValue = OtpErlangLong(100)
        val incrMsg = arrayOf<OtpErlangObject>(kPid, OtpErlangAtom("incr"), counterName, aValue)
        kProcess.send("erlcounters", "eNode", OtpErlangTuple(incrMsg))
        response = kProcess.receive()
        if (response is OtpErlangTuple) {
            val remoteCounterName = response.elementAt(1)
                .toString()
            val remoteCounterValue = response.elementAt(2)
                .toString()
            System.out.println(
                remoteCounterName + " = " +
                        remoteCounterValue)
        }
    } else {
        System.out.println("eNode is down")
    }
}
