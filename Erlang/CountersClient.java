import com.ericsson.otp.erlang.*;
public class CountersClient
{
  public static void main(String[] args) throws Exception
  {
    System.setProperty("OtpConnection.trace", "0");
    OtpNode javaNode = new OtpNode("jNode", "erljava");
    if (javaNode.ping("eNode@Pauls-MBP", 10000)) {
      System.out.println("eNode is up");
      OtpMbox jProcess = javaNode.createMbox();
      OtpErlangPid jPid = jProcess.self();
      jProcess.registerName("jProcess");

      // get value of my_counter
      OtpErlangObject get_atom = new OtpErlangAtom("get");
      OtpErlangObject counter_name = new OtpErlangList("my_counter");
      OtpErlangObject[] get_msg = {jPid, get_atom, counter_name};
      jProcess.send("erlcounters", "eNode", new OtpErlangTuple(get_msg));
      OtpErlangObject response = jProcess.receive();

      if (response instanceof OtpErlangTuple) {
        String remote_counter_name =
          ((OtpErlangString)((OtpErlangTuple)response).elementAt(1))
          .stringValue();
        int remote_counter_value =
          ((OtpErlangLong)((OtpErlangTuple)response).elementAt(2))
          .intValue();
        System.out.println(
          remote_counter_name + " = " +
          remote_counter_value);
      }
 
      // set my_counter to 1234
      OtpErlangObject aValue = new OtpErlangLong(1234);

      OtpErlangObject[] set_msg =
        {jPid, new OtpErlangAtom("set"), counter_name, aValue};
      jProcess.send("erlcounters", "eNode", new OtpErlangTuple(set_msg));
      response = jProcess.receive();
      if (response instanceof OtpErlangTuple) {
        String remote_counter_name =
          ((OtpErlangTuple) response).elementAt(1)
          .toString();
        String remote_counter_value =
          ((OtpErlangTuple) response).elementAt(2)
          .toString();
        System.out.println(
          remote_counter_name + " = " +
          remote_counter_value);
      }
 
      // increment my_counter by 10
      aValue = new OtpErlangLong(10);
      OtpErlangObject[] incr_msg =
        {jPid, new OtpErlangAtom("incr"), counter_name, aValue};
      jProcess.send("erlcounters", "eNode", new OtpErlangTuple(incr_msg));
      response = jProcess.receive();
      if (response instanceof OtpErlangTuple) {
        String remote_counter_name =
          ((OtpErlangTuple) response).elementAt(1)
          .toString();
        String remote_counter_value =
          ((OtpErlangTuple) response).elementAt(2)
          .toString();
        System.out.println(
          remote_counter_name + " = " +
          remote_counter_value);
      }
    }
    else {
      System.out.println("eNode is down");
    }
  }
}