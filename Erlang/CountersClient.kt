import com.ericsson.otp.erlang.*

    fun main(args: Array<String>) {
        System.setProperty("OtpConnection.trace", "0")
        val javaNode = OtpNode("jNode", "erljava")
        if (javaNode.ping("eNode", 10000)) {
            System.out.println("eNode is up")
            val jProcess = javaNode.createMbox()
            val jPid = jProcess.self()
            jProcess.registerName("jProcess")

            // get value of my_counter
            val get_atom = OtpErlangAtom("get")
            val counter_name = OtpErlangList("my_counter")
            val get_msg = arrayOf<OtpErlangObject>(jPid, get_atom, counter_name)
            jProcess.send("erlcounters", "eNode", OtpErlangTuple(get_msg))
            var response = jProcess.receive()

            if (response is OtpErlangTuple) {
                val remote_counter_name = ((response as OtpErlangTuple).elementAt(1) as OtpErlangString)
                        .stringValue()
                val remote_counter_value = ((response as OtpErlangTuple).elementAt(2) as OtpErlangLong)
                        .intValue()
                System.out.println(
                        remote_counter_name + " = " +
                                remote_counter_value)
            }

            // set my_counter to 1234
            var aValue: OtpErlangObject = OtpErlangLong(1234)

            val set_msg = arrayOf<OtpErlangObject>(jPid, OtpErlangAtom("set"), counter_name, aValue)
            jProcess.send("erlcounters", "eNode", OtpErlangTuple(set_msg))
            response = jProcess.receive()
            if (response is OtpErlangTuple) {
                val remote_counter_name = (response as OtpErlangTuple).elementAt(1)
                        .toString()
                val remote_counter_value = (response as OtpErlangTuple).elementAt(2)
                        .toString()
                System.out.println(
                        remote_counter_name + " = " +
                                remote_counter_value)
            }

            // increment my_counter by 10
            aValue = OtpErlangLong(10)
            val incr_msg = arrayOf<OtpErlangObject>(jPid, OtpErlangAtom("incr"), counter_name, aValue)
            jProcess.send("erlcounters", "eNode", OtpErlangTuple(incr_msg))
            response = jProcess.receive()
            if (response is OtpErlangTuple) {
                val remote_counter_name = (response as OtpErlangTuple).elementAt(1)
                        .toString()
                val remote_counter_value = (response as OtpErlangTuple).elementAt(2)
                        .toString()
                System.out.println(
                        remote_counter_name + " = " +
                                remote_counter_value)
            }
        } else {
            System.out.println("eNode is down")
        }
    }
