-module(erlcounters).
-export([start/0, init/0]).
start() ->
  register(erlcounters, spawn(?MODULE, init, [])).
init() ->
  loop(ets:new(erlcounters, [])).
%% note that we need to crate the table here, in the spawned process
loop(Counters) ->
  receive
    {From, get, Counter} ->
      Value = case ets:lookup(Counters, Counter) of
                [] -> 0;
                [{Counter,V}] -> V
              end,
      From ! {self(), Counter, Value},
      loop(Counters);
    {From, set, Counter, Value} ->
      ets:insert(Counters, {Counter, Value}),
      From ! {self(), Counter, Value},
      loop(Counters);
    {From, incr, Counter, Increment} ->
      Value = case ets:lookup(Counters, Counter) of
                [] -> 0;
                [{Counter,V}] -> V
              end,
      ets:insert(Counters, {Counter, Value+Increment}),
      From ! {self(), Counter, Value+Increment},
      loop(Counters);
    stop ->
      io:format("gotta stop~n", []),
      ok
  end.